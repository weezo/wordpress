#!/usr/bin/env bash

# ---------------------------------------------------------------------------------
# Apontar arquivo hosts para o IP da maquina
# ---------------------------------------------------------------------------------

#-------------------
# Windows
#-------------------
# Alterar o arquivo C:\Windows\System32\drivers\etc\hosts com editor (ex.: bloco de notas) executando como administrador
#Acrescentar a linha:
192.168.50.15	wp.dev

#-------------------
# Ubuntu 
#-------------------
# Alterar o arquivo /etc/hosts com editor. 
sudo gedit /etc/hosts
# Acrescentar a linha:
192.168.50.15	wp.dev


# ---------------------------------------------------------------------------------
# Banco de dados MySQL
# ---------------------------------------------------------------------------------
# Host: 192.168.50.15 (IP de acesso á máquina)
# Port: 3306
# User: root
# Senha: root
# ---------------------------------------------------------------------------------

#-------------------
# Habilitar acesso fora do vagrant
#-------------------
# Alterar o arquivo de configurações, trocando bind-address para 0.0.0.0
sudo nano /etc/mysql/my.cnf 
# Habilitar permissões de acesso
mysql -u root -proot -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'root' WITH GRANT OPTION; FLUSH PRIVILEGES;"
# Reiniciar
sudo service mysql restart

#-------------------
# Criar banco
#-------------------
mysql -u root -p
SHOW DATABASES;
CREATE DATABASE wordpress;
SHOW DATABASES;
\q 

