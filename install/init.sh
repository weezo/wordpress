#!/usr/bin/env bash

# Variaveis
DBNAME=${1}
DBPREFIX=${2}
WPURL=${3}
WPDIR=${4}
WPLANG=${5}
WPTITLE=${6}
WPUSER=${7}
WPPASS=${8}
WPEMAIL=${9}
PLDIR=${10}
PLNAMESPC=${11}

#Atualizar apts
sudo apt-get update

# Criar diretórios e dar permissões
sudo chmod -R 755 /var/www
sudo mkdir -p /var/www/${WPDIR}
sudo chown -R vagrant:vagrant /var/www
sudo chown -R vagrant:vagrant /var/www/${WPDIR}
sudo chmod -R g+rwX /var/www
sudo rm -rf /var/www/html

#Copia o .gitignore para a pasta do wordpress
sudo cp /var/www/install/.gitignore /var/www/${WPDIR}

# Pacote de idiomas
sudo apt-get install language-pack-pt -y

# php7
sudo apt-get install -y vim curl python-software-properties
sudo add-apt-repository -y ppa:ondrej/php
sudo apt-get update
sudo apt-get install -y php7.0

# Apache2
sudo add-apt-repository -y ppa:ondrej/apache2
sudo apt-get update
sudo apt-get install -y apache2

# MySQL 5.5
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'
sudo add-apt-repository -y ppa:ondrej/mysql-5.5
sudo apt-get update
sudo apt-get install -y mysql-server-5.5
sudo apt-get install -y mysql-server
mysql -u root -proot -e "CREATE DATABASE ${DBNAME};"

# Módulos php7
sudo apt-get install -y libapache2-mod-php7.0 php7.0-mysql php7.0-curl php7.0-json

# Vhost
sudo cp /var/www/install/sample.conf /etc/apache2/sites-available/${WPURL}.conf
sed -i "s/SAMPLE/${WPURL}/g" /etc/apache2/sites-available/${WPURL}.conf
sed -i "s/FOLDER/${WPDIR}/g" /etc/apache2/sites-available/${WPURL}.conf
sudo a2ensite ${WPURL}.conf
service apache2 restart

# xdebug
cat << EOF | sudo tee -a /etc/php/7.0/mods-available/xdebug.ini
xdebug.scream=1
xdebug.cli_color=1
xdebug.show_local_vars=1
EOF

sudo a2enmod rewrite

# Configura para exibir todos os erros
sudo sed -i "s/error_reporting = .*/error_reporting = E_ALL/" /etc/php/7.0/apache2/php.ini
sudo sed -i "s/display_errors = .*/display_errors = On/" /etc/php/7.0/apache2/php.ini
sudo sed -i "s/disable_functions = .*/disable_functions = /" /etc/php/7.0/cli/php.ini

# Restart apache
sudo service apache2 restart

# Atualiza todos os apts
apt-get update -y

# Composer
php -r "readfile('https://getcomposer.org/installer');" > composer-setup.php
php -r "if (hash('SHA384', file_get_contents('composer-setup.php')) === '781c98992e23d4a5ce559daf0170f8a9b3b91331ddc4a3fa9f7d42b6d981513cdc1411730112495fbf9d59cffbf20fb2') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); }"
php composer-setup.php
php -r "unlink('composer-setup.php');"
sudo mv composer.phar /usr/local/bin/composer

# Git
sudo apt-get install -y git

# WordPress Coding Standards for PHP_CodeSniffer
composer create-project wp-coding-standards/wpcs:dev-master --no-dev --keep-vcs
sudo ln -s /home/vagrant/wpcs/vendor/bin/phpcs /usr/local/bin/phpcs
sudo ln -s /home/vagrant/wpcs/vendor/bin/phpcbf /usr/local/bin/phpcbf

# WP Cli
curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
chmod +x wp-cli.phar
sudo mv wp-cli.phar /usr/local/bin/wp

# Baixar e instalar o Wordpress
wp core download --path=/var/www/${WPDIR} --locale=${WPLANG} --allow-root
wp core config --path=/var/www/${WPDIR} --dbname=${DBNAME} --dbuser=root --dbpass=root --dbprefix=${DBPREFIX} --locale=${WPLANG} --extra-php --allow-root <<PHP
define( 'WP_DEBUG', true );
define( 'WP_DEBUG_LOG', true );
PHP
rm /var/www/${WPDIR}/wp-config-sample.php
wp core install --path=/var/www/${WPDIR} --url=http://${WPURL} --title=${WPTITLE} --admin_user=${WPUSER} --admin_password=${WPUSER} --admin_email=${WPEMAIL} --skip-email --allow-root

# Baixar framework, configurar namespace e criar pastas para desenvolvimento do plugin
if [ "${PLDIR}" != "N" ]; then
	git clone https://github.com/getherbert/herbert.git
	sed -i "s/MyPlugin/${PLNAMESPC}/g" herbert/composer.json
	sed -i "s/MyPlugin/${PLNAMESPC}/g" herbert/herbert.config.php
	sed -i "s/MyPlugin/${PLNAMESPC}/g" herbert/app/Helper.php
	sed -i "s/MyPlugin/${PLNAMESPC}/g" herbert/app/api.php
	sed -i "s/MyPlugin/${PLNAMESPC}/g" herbert/app/enqueue.php
	sed -i "s/MyPlugin/${PLNAMESPC}/g" herbert/app/panels.php
	sed -i "s/MyPlugin/${PLNAMESPC}/g" herbert/app/routes.php
	sed -i "s/MyPlugin/${PLNAMESPC}/g" herbert/app/shortcodes.php
	sed -i "s/MyPlugin/${PLNAMESPC}/g" herbert/app/widgets.php
	mv herbert /var/www/${WPDIR}/${PLDIR}
	sudo ln -s /var/www/${WPDIR}/${PLDIR} /var/www/${WPDIR}/wp-content/plugins/${PLDIR}
	cd /var/www/${WPDIR}/${PLDIR}
	rm -rf .git
	rm .gitignore
	rm .editorconfig
	composer install
	cd
fi
sudo rm -rf /var/www/html

#Ruby + Gems
sudo apt-get install -y git-core curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev python-software-properties libffi-dev libgdbm-dev libncurses5-dev automake libtool bison libffi-dev
command curl -sSL https://rvm.io/mpapis.asc | gpg --import -
curl -L https://get.rvm.io | bash -s stable
umask u=rwx,g=rwx,o=rx
source /etc/profile.d/rvm.sh
rvm requirements
rvm install 2.2.3
rvm use 2.2.3 --default
echo "gem: --no-ri --no-rdoc" > ~/.gemrc
gem install rubygems-update
update_rubygems
gem install bundler

# Wordmove para deploy
gem install wordmove
cd /var/www/${WPDIR}
wordmove init
cd
sed -i "s/vhost.local/${WPURL}/" /var/www/${WPDIR}/Movefile