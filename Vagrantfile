# -*- mode: ruby -*-
# vi: set ft=ruby :

require 'yaml'

# Configurações do Vagrant
VAGRANTFILE_API_VERSION = "2"
Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

    _conf = YAML.load(
    File.open(
      File.join(File.dirname(__FILE__), 'install/default-site.yml'),
      File::RDONLY
    ).read
  )

  if File.exists?(File.join(File.dirname(__FILE__), 'site.yml'))
    _site = YAML.load(
      File.open(
        File.join(File.dirname(__FILE__), 'site.yml'),
        File::RDONLY
      ).read
    )
    _conf.merge!(_site) if _site.is_a?(Hash)
  end


  # Box a ser utilizada
  config.vm.box = _conf['box']

  # Verificar se a box esta atualizada
  config.vm.box_check_update = true

  # Config da Virtual box
  config.vm.provider :virtualbox do |vb|
    vb.name = _conf['vb_name']
    vb.customize ["modifyvm", :id, "--memory", _conf['memory']]
    vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
    vb.customize ["modifyvm", :id, "--natdnsproxy1", "on"]
  end

  # Permite usar SSH Keys na maquina HOST dentro da GUEST
  config.ssh.forward_agent = true

  # Endereço a ser acessado
  config.vm.hostname = _conf['hostname']

  # IP para a VM
  config.vm.network "private_network", ip: _conf['ip']
  config.vm.network "forwarded_port", guest: 3306, host: 33060

  #Configurações para o plugin Hosts Updater
  if Vagrant.has_plugin?('vagrant-hostsupdater')
    config.hostsupdater.aliases = [_conf['wp_url']]
    config.hostsupdater.remove_on_suspend = true
  end

  # Nome do user da VM (ex.: default)
  config.vm.define _conf['vb_user'] do |v|
  end

  # Pastas a sincronizar
  config.vm.synced_folder ".", "/var/www", :id => "www-root", :create => "true", :mount_options => ['dmode=755', 'fmode=755'], :owner => "vagrant", :group => "vagrant"

  # Scripts para o GUEST
  config.vm.provision :shell, :path => "install/init.sh", :args => [ 
    _conf['db_name'],   _conf['db_prefix'], 
    _conf['wp_url'],      _conf['wp_dir'],      _conf['wp_lang'],       _conf['wp_title'], 
    _conf['admin_user'],  _conf['admin_pass'],  _conf['admin_email'], 
    _conf['pl_dir'],      _conf['pl_namespace']
  ]

end